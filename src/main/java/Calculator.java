public class Calculator {
    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static float divide(int a, int b) {
        float x = (float) a / (float) b;
        if (b != 0) {
            return x;
        } else {
            System.out.println("Division by 0");
            return 0;
        }
    }

    public static int power(int a, int b) {
        return (int) Math.pow(a, b);
    }
}
