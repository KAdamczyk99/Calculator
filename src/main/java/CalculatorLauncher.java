import java.util.Scanner;

public class CalculatorLauncher {
    public static void main(String[] args) {
        Scanner scan1 = new Scanner(System.in);
        int a = scan1.nextInt();
        int b = scan1.nextInt();
        String operationType = scan1.next();
        if (operationType.equals("+")) {
            System.out.println(Calculator.add(a, b));
        } else if (operationType.equals("-")) {
            System.out.println(Calculator.subtract(a, b));
        } else if (operationType.equals("*")) {
            System.out.println(Calculator.multiply(a, b));
        } else if (operationType.equals("/")) {
            System.out.println(Calculator.divide(a, b));
        } else if (operationType.equals("^")) {
            System.out.println(Calculator.power(a, b));
        } else {
            System.out.println("Wrong input");
        }
    }
}

